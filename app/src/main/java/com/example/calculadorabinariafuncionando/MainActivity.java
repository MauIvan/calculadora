package com.example.calculadorabinariafuncionando;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    CheckBox check1, check2, check3, check4, check5, check6, check7, check8,check9,check10;
    TextView Imprimir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        check1 = findViewById(R.id.Check1);
        check2 = findViewById(R.id.Check2);
        check3 = findViewById(R.id.Check3);
        check4 = findViewById(R.id.Check4);
        check5 = findViewById(R.id.Check5);
        check6 = findViewById(R.id.Check6);
        check7 = findViewById(R.id.Check7);
        check8 = findViewById(R.id.Check8);
        check9 = findViewById(R.id.Check9);
        check10 = findViewById(R.id.Check10);
        Imprimir = findViewById(R.id.Resultados);




    }


    public void Even1(View view) {
        String Resultado;
        int suma = 0;
        int Check = 0;

        Check += (check1.isChecked()) ? 1 : 0;
        Check += (check2.isChecked()) ? 2 : 0;
        Check += (check3.isChecked()) ? 4 : 0;
        Check += (check4.isChecked()) ? 8 : 0;
        Check += (check5.isChecked()) ? 16 : 0;
        Check += (check6.isChecked()) ? 32 : 0;
        Check += (check7.isChecked()) ? 64 : 0;
        Check += (check8.isChecked()) ? 128 : 0;
        Check += (check9.isChecked()) ? 256 : 0;
        Check += (check10.isChecked()) ? 512 : 0;

        suma = suma + Check;
        Resultado = String.valueOf(suma);
        Imprimir.setText(Resultado);

    }
}
